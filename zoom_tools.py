import jwt
import requests
import json
import datetime
import calendar
from datetime import date
from time import time

API_KEY = 'BdqLpVlKT7GfV-ybVQL_Xg'
API_SEC = 'gCAUrPgWS6IFsorE43Twg8gCprdicSEbWihU'

DAY_NAMES = [day for day in calendar.day_name]


def get_dates_of_month_by_day_name(day_name, month=datetime.datetime.now().month, year=datetime.datetime.now().year):
    possible_dates = []
    if day_name not in DAY_NAMES:
        print(f"uh oh! you passed invalid argument {day_name} as 'day_name' to get_dates_of_month_by_day_name")
    else:
        index_day_name_in_week = DAY_NAMES.index(day_name)
        possible_dates = [
            week[index_day_name_in_week]
            for week in calendar.monthcalendar(year, month)
            if week[index_day_name_in_week]]
    return possible_dates


def get_next_cec_date(day_name='Tuesday', occurrence_in_month=2):
    current_time = datetime.datetime.now()
    year = current_time.year
    month = current_time.month
    possible_dates = get_dates_of_month_by_day_name(day_name)
    event_date = date(year, month, possible_dates[occurrence_in_month - 1])
    # if this has already happened, return same thing for next month
    already_happened = bool(event_date < current_time.date())
    if already_happened:
        month += 1
        possible_dates = get_dates_of_month_by_day_name(day_name)
        event_date = date(year, month, possible_dates[occurrence_in_month - 1])
    return event_date


def generate_token():
    token = jwt.encode(

        # Create a payload of the token containing
        # API Key & expiration time
        {'iss': API_KEY, 'exp': time() + 5000},

        # Secret used to generate token signature
        API_SEC,

        # Specify the hashing alg
        algorithm='HS256'
    )
    return token


def get_meeting_details(page_size=300, next_page_token='', event_type='scheduled', page_number=1):
    # user_id = 'Zx6NHAJ1Q-GcZ95RhE2XWQ'
    user_id = 'me'
    next_cec_meeting = get_next_cec_date()
    next_meeting = datetime.datetime(year=2022,month=5,day=24)
    upcoming_meetings_url = f'https://api.zoom.us/v2/users/{user_id}/meetings?page_size=300'
    headers = {'authorization': 'Bearer ' + generate_token(),
               'content-type': 'application/json'}

    correct_params = {
        # 'userId': user_id,
        'type': f'{event_type}',
        'page_size': page_size,
        'next_page_token': f'{next_page_token}',
        # 'page_number': page_number
    }
    print(correct_params)
    data = json.dumps(correct_params)
    r = requests.get(upcoming_meetings_url, headers=headers, data=data)
    print(f'response: {r.text}')
    response = json.loads(r.text)
    print(response)
    meetings = response['meetings']
    print(meetings)
    for m in meetings:
        topic = m['topic']
        start_time = datetime.datetime.strptime(m['start_time'], '%Y-%m-%dT%H:%M:%SZ')
        date = start_time.date()
        is_same_year = bool (date.year == next_cec_meeting.year)
        is_same_month = bool (date.month == next_cec_meeting.month)
        is_same_day = bool(date == next_cec_meeting)
        if is_same_day:
            print('same year', date, topic)
        # if date == next_cec_meeting:
        # print(date, is_same_day, topic, type(next_cec_meeting))

    # r = requests.post(
    #     f'https://api.zoom.us/v2/report/upcoming_events',
    #     headers=headers, data=json.dumps())

    # print(r.text)
    # converting the output into json and extracting the details
    # y = json.loads(r.text)



get_meeting_details()