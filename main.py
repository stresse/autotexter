import datetime

import requests
import pandas as pd
import re
# import smsitai as sms
import smsitai
from pyzoom import ZoomClient


SERVER = 'https://www.smsit.ai/smsgateway'
API_KEY = '9b96bd0d32b92de50541e6682e10a97d139f3783'
PATH_TO_PRECINCT_CHAIRS = '/Users/jesse/Documents/coding/python/autotexter/BCDP EXECUTIVE COMMITTEE ROSTER. 4.11.2022. alpha order.3.xlsx'
PATH_TO_REGISTERED = '/Users/jesse/Documents/coding/python/autotexter/84115881703_RegistrationReport.csv'
REGISTRATION_URL = 'https://us02web.zoom.us/meeting/register/tZAsduygpjotGteMXeTHw_nrudddxAAVlwIf'
VERBOSE_MODE = True

ZOOM_API_KEY = 'BdqLpVlKT7GfV-ybVQL_Xg'
ZOOM_API_SECRET = 'gCAUrPgWS6IFsorE43Twg8gCprdicSEbWihU'

smsIt = smsitai.SMSit(API_KEY)
# smsitai.send.SMSit(API_KEY)


def get_zoom_info():
    client = ZoomClient(ZOOM_API_KEY, ZOOM_API_SECRET)
    meetings = client.meetings.list_meetings()
    for m in meetings:
        print(f"start_time: {m.start_time}\ttopic: {m.topic}")

# def send_message(number, message, schedule=None, devices=0, isMMS=False, attachments=None, prioritize=False):
#     url = f'{SERVER}/services/send.php'
#     message_type = 'mms' if isMMS else 'sms'
#     message_priority = 1 if prioritize else 0
#     post_data = dict(number=number,
#                      message=message,
#                      schedule=schedule,
#                      key=API_KEY,
#                      devices=devices,
#                      type=message_type,
#                      attachments=attachments,
#                      prioritize=message_priority)
#     r = requests.post(url, data=post_data)
#     text = r.text.replace('true', 'True').replace('false', 'False').replace('null', 'None')
#     response = eval(text)
#     messages = response['messages'] if 'messages' in response.keys() else {}



def get_first_name(full_name):
    if full_name:
        prefixes = ['dr', 'mr', 'miss', 'mrs', 'sra', 'sr', r'sdec\s*\d{2}']
        regex = fr'(?ims)^({"|".join(prefixes)})(\.)?\s'
        just_name = re.sub(regex, '', full_name)
        first_name = just_name.split(' ')[0]
        return first_name
    else:
        return ''


def get_clean_invite_list() -> pd.DataFrame:
    people_to_invite = pd.read_excel(PATH_TO_PRECINCT_CHAIRS, header=1).fillna('')
    initial_rows = people_to_invite.shape[0]
    people_to_invite = people_to_invite.loc[people_to_invite['Last Name'] != '']
    people_to_invite['First Name'] = [get_first_name(i) for i in people_to_invite['Preferred Name']]
    num_rows_with_name = people_to_invite.shape[0]
    log(
        f'removing {initial_rows - num_rows_with_name} rows '
        f'({initial_rows} --> {num_rows_with_name}) '
        f'because they have no name')
    people_to_invite = people_to_invite.loc[people_to_invite['Cell'] != '']
    num_people_with_cell = people_to_invite.shape[0]
    log(
        f'removing {num_rows_with_name - num_people_with_cell} '
        f'people ({num_rows_with_name} --> {num_people_with_cell}) '
        f'people for not having a cell phone number')
    return people_to_invite


def get_people_to_invite():
    registered_people = pd.read_csv(PATH_TO_REGISTERED).fillna('')
    invite_list = get_clean_invite_list()
    names_to_invite = list(invite_list[['First Name', 'Last Name']].to_records(index=False))
    invite_list['Already Registered'] = [
        registered_people.loc[
            (registered_people['Last Name'] == last_name) &
            (registered_people['First Name'] == first_name)
            ].shape[0] > 0 for first_name, last_name in names_to_invite]
    start_rows = invite_list.shape[0]
    people_to_invite = invite_list.loc[invite_list['Already Registered'] == False]
    end_rows = people_to_invite.shape[0]
    log(f'removing {start_rows - end_rows} '
        f'people ({start_rows} --> {end_rows}) '
        f'people because they are already registered')
    return people_to_invite


def send_reminder_texts():
    people_to_invite = get_people_to_invite()
    for person in people_to_invite[
        ['First Name', 'Last Name', 'Preferred Name', 'E-Mail', 'Cell']
    ].itertuples(index=False):
        first_name, last_name, preferred_name, email, cell = person
        # log(f'changing cell from {cell} to (210) 310-9760')
        cell = '(210) 310-9760'
        cell = re.sub(rf'(?ims)[\s\-()]', '', cell)
        message = f"Hi {first_name}, this is Jesse (the guy volunteering for the BCDP).\n\n" \
                  f"I am texting to remind you about tonight's CEC meeting.\n\n" \
                  f"Please pre-register for the meeting at {REGISTRATION_URL}"
        # send_message(cell, message)


def log(entry):
    if VERBOSE_MODE is True:
        print(entry)

messages = []
for i in range(0, 1):
    n = '(210) 310-9760'
    m = f'test #{i}\nsent:{datetime.datetime.now()}'
    messages.append(dict(number=n, message=m))
    # print(f'SENDING MESSAGE:\ti={i}\tnumber: {n}\tmessage: {m}')
    # send_message(n, m)
    # get_phone_number(n)
smsIt.send_messages(messages)

if __name__ == '__main__':
    pass
    # send_reminder_texts()
    # get_zoom_info()
    # send_message('2103109760', 'this is a test')
    # check_if_registered('Jesse')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
